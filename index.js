

// #3

fetch('https://jsonplaceholder.typicode.com/todos',{method:'GET'})
		.then((response) => response.json())
		.then((json) => console.log(json))
		

// #4    
fetch('https://jsonplaceholder.typicode.com/todos',{method:'GET'})
		.then((response) => response.json())
		.then((json) => {
			let list = json.map((todo=> {
				return todo.title;
			}))
			console.log(list);
		})


		
		
		
	
// #5
fetch('https://jsonplaceholder.typicode.com/todos/1')
		.then((response) => response.json())
		.then((json) => console.log(json))
	
//#6
fetch('https://jsonplaceholder.typicode.com/todos/1')
		.then((response) => response.json())
		.then((json) => console.log(json.title, json.completed))

//#7
	fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			userId: 1,
			id: 1,
			title: 'A new Post',
			completed: false
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//#8
	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			userId: 1,
			id: 1,
			title: 'A new Post to re:Post',
			completed: true
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//#9

	fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			userId: 1,
			id: 1,
			title: 'A new Post to re:Post: Postception',
			description :' third edit',
			completed: true,
			dateCompleted: 'October, 25, 2021'
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//#10

	fetch('https://jsonplaceholder.typicode.com/todos/14', {
		method: 'PATCH',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			
			title: 'revised Edition of original',
			
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))

//#11

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			
			completed: true,
			dateOfCompletion: "October 25,2021"
			
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json))